/**
 * Controller for the registration page
 *
 * @author Jonathan van Veen
 */
function registrationController() {

    var registrationView;

    function initialize() {
        $.get("views/registration.html")
            .done(setup)
            .fail(error);
    }

    function setup(registration) {
        registrationView = $(registration);

        //register_form
        registrationView.find(".register_form").on("submit", handleRegistration);

        $(".container").empty().append(registrationView);

        return false;
    }

    function handleRegistration() {
        var firstname = registrationView.find("[name='firstnameInput']").val();
        var lastname = registrationView.find("[name='lastnameInput']").val();
        var username = registrationView.find("[name='usernameInput']").val();
        var emailadress = registrationView.find("[name='emailaddressInput']").val();
        var password = registrationView.find("[name='passwordInput']").val();
        var passwordRepeat = registrationView.find("[name='passwordRepeat']").val();
        var birthdate = registrationView.find("[name='birthdateInput']").val();
        var gender = registrationView.find("[name='genderInput']").val();
        var picture = "src/assets/img/user_icon.jpg";

        //check voor password = password repeat
        if (password === passwordRepeat && passwordRepeat !== undefined) {
            console.log("De wachtwoorden komen overeen");
        } else {
            registrationView
                .find(".error")
                .html("U heeft het wachtwoord verkeerd ingevoerd!");
        }
        // aray alle gegevens
        var registration = [firstname, lastname, username, emailadress, password, birthdate, gender, picture];

        console.log("registration", registration);

        databaseManager
            .query("INSERT INTO `user` (firstname, lastname, username, email, password, birthdate, gender, picture) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", [registration])
            .done(function () {
                window.alert("U heeft nu een account gemaakt. Als u uw account verder wilt maken kunt u naar settings gaan.\n" +
                    "Als u dat heeft gedaan kunt u volledig aan de slag gaan met uw account.");
                loadController(CONTROLLER_HOME)
            }).fail(function (reason) {
            console.log(reason);
            registrationView
                .find(".error")
                .html("Er is iets misgegaan??? :'(");
        })
    }

    function error() {
        $(".container").html("Failed to load Registration!")
    }

    initialize()
}