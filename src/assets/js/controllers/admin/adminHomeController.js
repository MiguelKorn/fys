/**
 * Controller for the index page
 *
 * @author Jonathan van Veen
 */
function adminHomeController() {

    var adminHomeView;

    function initialize() {
        $.get("views/admin/home.html")
            .done(setup)
            .fail(error);
    }

    function setup(data) {
        adminHomeView = $(data);

        $(".container").empty().append(adminHomeView);

    }

    function error() {
        $(".container").html("Failed to load Home!")

    }

    initialize()
}