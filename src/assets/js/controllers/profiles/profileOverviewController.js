function profileOverviewController() {
    var profileOverviewView;
    var profileTileTemplate;

    function initialize() {
        var profileOverviewRequest = $.get("views/profiles/profile_details.html");
        var profileTileRequest = $.get("views/templates/profile_tile.html");

        if (session.get("status") === "logged in" || session.get("userId") !== "test") {
            $.when(profileOverviewRequest, profileTileRequest)
                .done(function(profileOverviewResponse, profileTileResponse) {
                    setup(profileOverviewResponse[0], profileTileResponse[0]);
                })
                .fail(error);
        } else {
            window.alert("Om bij dit gedeelte van de website te komen moet u ingelogd zijn. Maak hierbij een account aan door op registreren te klikken.")
        }

    }

    function setup(profileOverviewResponse, profileTileResponse) {
        //load the manual-content into memory
        profileOverviewView = $(profileOverviewResponse);
        profileTileTemplate = $(profileTileResponse);

        profileOverviewView.find(".hobby_form").on("checkbox", function () {
            handleFilterHobby();
        });
        profileOverviewView.find(".age_form").on("checkbox", function () {
            handleFilterAge();
        });
        profileOverviewView.find(".destination_form").on("checkobx", function () {
            handleFilterDestination();
        });
        profileOverviewView.find(".gender_form").on("checkbox", function () {
            handleFilterGender();
        });

        loadProfiles();
    }

    function loadProfiles() {
        databaseManager
            .query("SELECT username, bio, picture FROM `user`")
                .done(displayProfiles)
                .fail(error);
    }

    function displayProfiles(data) {
        for (var i = 0; i < data.user.length; i++) {
            var user = data.user[i];

            var profileTile = profileTileTemplate.clone();

            profileTile.find(".profile_img").css("background-image", "url('" + profile.picture + "'");
            profileTile.find(".profile_header").html(user.username);
            profileTile.find(".profile_body").html(user.bio);

            profileTile.data("user", user);

            profileOverviewView.find(".profiles").append(profileTile);
        }
        $(".container").empty().append(profileOverviewView);
    }

    // function handleFilterHobby() {
    //
    //     var hobby = profileOverviewView.find("[name='hobby']");
    //
    //     //queries for filter
    //     databaseManager
    //         .query("SELECT hobby FROM `interest` WHERE `hobby` = ?", [hobby])
    //         .done(function (data) {
    //             for (var i = 0; i < data.profiles.length; i++) {
    //                 var profile = data.profiles[i];
    //
    //                 var profileTile = profileTileTemplate.clone();
    //
    //
    //             }
    //         });
    // }
    //
    // function handleFilterAge() {
    //
    //     var age = profileOverviewView.find("[name='age']");
    //     var age2 = age + 10;
    //
    //     databaseManager
    //         .query("SELECT `birthdate` FROM `user` WHERE `birthdate` BETWEEN ? AND ?", [age] [age2])
    //         .done(function (data) {
    //
    //         });
    // }
    //
    // function handleFilterDestination() {
    //     var destination = profileOverviewView.find("[name='destination']");
    //
    //     databaseManager
    //         .query("SELECT `destination` FROM `user` WHERE `destination` = ?" [destination])
    //         .done(function(data){
    //
    //
    //         });
    //
    // }
    //
    // function handleFilterGender() {
    //     var gender = profileDetailView.find("[name='gender']");
    //
    //     databaseManager
    //         .query("SELECT `gender` FROM `user` WHERE `gender` = ?" [gender])
    //         .done(function(data) {
    //
    //         });
    // }

    $('.favorite_btn').on('click', function(e) {
        $(this).toggleClass('far');
        $(this).toggleClass('fas');
    });

    $('.filterContent_btn').on('click', function(e) {
        $('.filter_container').show();
    });

    $('.filter_head').on('click', function(e) {
        $('.filter_container').hide();
    });

    function error() {
        $(".container").html("Failed to load content!")
    }

    function doPage() {
        var page;

        page = document.getElementById('cb1');
        if (page.selected) {
            show(   document.getElementById('p1'),
                document.getElementById('p2'),
                document.getElementById('p3'),
                document.getElementById('p4'),
                document.getElementById('p5')
            );
            hide(   document.getElementById('p6'),
                document.getElementById('p7'),
                document.getElementById('p8'),
                document.getElementById('p9'),
                document.getElementById('p10'),
                document.getElementById('p11'),
                document.getElementById('p12'),
                document.getElementById('p13'),
                document.getElementById('p14'),
                document.getElementById('p15'),
                document.getElementById('p16'),
                document.getElementById('p17'),
                document.getElementById('p18'),
                document.getElementById('p19'),
                document.getElementById('p20')
            );
            return;
        }

        page = document.getElementById('cb2');
        if (page.selected) {
            show(   document.getElementById('p1'),
                document.getElementById('p2'),
                document.getElementById('p3'),
                document.getElementById('p4'),
                document.getElementById('p5'),
                document.getElementById('p6'),
                document.getElementById('p7'),
                document.getElementById('p8'),
                document.getElementById('p9'),
                document.getElementById('p10')
            );
            hide(   document.getElementById('p11'),
                document.getElementById('p12'),
                document.getElementById('p13'),
                document.getElementById('p14'),
                document.getElementById('p15'),
                document.getElementById('p16'),
                document.getElementById('p17'),
                document.getElementById('p18'),
                document.getElementById('p19'),
                document.getElementById('p20')
            );
            return;
        }


        page = document.getElementById('cb3');
        if (page.selected) {
            show(   document.getElementById('p1'),
                document.getElementById('p2'),
                document.getElementById('p3'),
                document.getElementById('p4'),
                document.getElementById('p5'),
                document.getElementById('p6'),
                document.getElementById('p7'),
                document.getElementById('p8'),
                document.getElementById('p9'),
                document.getElementById('p10'),
                document.getElementById('p11'),
                document.getElementById('p12'),
                document.getElementById('p13'),
                document.getElementById('p14'),
                document.getElementById('p15')
            );

            show(   document.getElementById('p16'),
                document.getElementById('p17'),
                document.getElementById('p18'),
                document.getElementById('p19'),
                document.getElementById('p20')
            );
            return;
        }


        page = document.getElementById('cb4');
        if (page.selected) {
            show(   document.getElementById('p1'),
                document.getElementById('p2'),
                document.getElementById('p3'),
                document.getElementById('p4'),
                document.getElementById('p5'),
                document.getElementById('p6'),
                document.getElementById('p7'),
                document.getElementById('p8'),
                document.getElementById('p9'),
                document.getElementById('p10'),
                document.getElementById('p11'),
                document.getElementById('p12'),
                document.getElementById('p13'),
                document.getElementById('p14'),
                document.getElementById('p15'),
                document.getElementById('p16'),
                document.getElementById('p17'),
                document.getElementById('p18'),
                document.getElementById('p19'),
                document.getElementById('p20')
            );
        }
    }

    //Run the initialize function to kick things off
    initialize();
}