function buddyTrackerController() {

    var buddyTrackerView;

    function initialize() {
        $.get("views/profiles/buddy_tracker.html")
            .done(setup)
            .fail(error);
    }

    function setup(data) {
        //load the manual-content into memory
        buddyTrackerView = $(data);

        $(".container").empty().append(buddyTrackerView);
    }

    function error() {
        $(".container").html("Failed to load content!")
    }

    //Run the initialize function to kick things off
    initialize();
}