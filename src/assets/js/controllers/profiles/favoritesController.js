function favoritesController() {

    var favoritesView;

    function initialize() {
        $.get("views/profiles/favorites.html")
            .done(setup)
            .fail(error);
    }

    function setup(data) {
        //load the manual-content into memory
        favoritesView = $(data);

        $(".container").empty().append(favoritesView);
    }

    $('.favorite_btn').on('click', function(e) {
        $(this).toggleClass('far');
        $(this).toggleClass('fas');
    });

    $('.filterContent_btn').on('click', function(e) {
        $('.filter_container').show();
    });

    $('.filter_head').on('click', function(e) {
        $('.filter_container').hide();
    });

    function error() {
        $(".container").html("Failed to load content!")
    }

    //Run the initialize function to kick things off
    initialize();
}