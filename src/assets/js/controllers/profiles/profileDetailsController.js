function profileDetailsController() {

    var profileDetailView;

    function initialize() {
        $.get("views/profiles/profile_details.html")
            .done(setup)
            .fail(error);
    }

    function setup(data) {
        //load the manual-content into memory
        profileDetailView = $(data);

        profileDetailView.find(".hobby_form").on("checkbox", function () {
            handleFilter();

        $(".container").empty().append(profileDetailView);
    });

    function handleFilter() {

            var hobby = profileDetailView.find("[name='hobby']");
            var age = profileDetailView.find("[name='age']");
            var destination = profileDetailView.find("[name='destination']");
            var gender = profileDetailView.find("[name='gender']");

            //queries for filter
            // databaseManager
            //     .query("SELECT hobby FROM `interest` WHERE `hobby` = ?", [hobby])
            //     .done(function (data) {
            //
            //     });
            //
            // databaseManager
            //     .query("SELECT `birthdate` FROM `user` WHERE `birthdate` BETWEEN '1968-01-01' AND '1999-12-31'")
            //     .done(function(data) {
            //
            //     });
            //
            // databaseManager
            //     .query("SELECT `destination` FROM `user` WHERE `destination` = ?" [destination])
            //     .done(function(data){
            //
            //
            //     });
            //
            // databaseManager
            //     .query("SELECT `gender` FROM `user` WHERE `gender` = ?" [gender])
            //     .done(function(data) {
            //
            //     })
        }




    $('.favorite_btn').on('click', function(e) {
        $(this).toggleClass('far');
        $(this).toggleClass('fas');
    });

    $('.filterContent_btn').on('click', function(e) {
        $('.filter_container').show();
    });

    $('.filter_head').on('click', function(e) {
        $('.filter_container').hide();
    });

    function error() {
        $(".container").html("Failed to load content!")
    }

    //Run the initialize function to kick things off
    initialize();
}