/**
 * Responsible for handeling the actions on the review pages
 *
 *  @author Mike Korver
 */
function reviewsController() {
    //Reference to our loaded view
    var reviewView;

    function initialize() {
        $.get("views/reviews.html")
            .done(setup)
            .fail(error);
    }
    // Called when the reviews.html has been loaded
    function setup(reviews) {
        //load the reviews-content into memory
        reviewView = $(reviews);

        reviewView.find(".review_send").on("submit", function() {
            handleReview();

            return false;
        });



        //document.getElementById("p").innerHTML = "new text!";

        //reviewView.find(".review_site").on();

            var buttonSite = reviewView.find(".review_site_button");
            var buttonTrip = reviewView.find(".review_trip_button");
            var buttonCreate = reviewView.find(".review_create_button");

            var containerSite = reviewView.find(".review_site");
            var containerTrip =  reviewView.find(".review_trip");
            var containerCreate =  reviewView.find(".review_create");

            buttonSite.on('click', function (e) {
                buttonSite.addClass('active');
                buttonTrip.removeClass('active');
                buttonCreate.removeClass('active');

                containerSite.removeClass('hide');
                containerTrip.addClass('hide');
                containerCreate.addClass('hide');

            });

            buttonTrip.on('click', function (e) {
                buttonSite.removeClass('active');
                buttonTrip.addClass('active');
                buttonCreate.removeClass('active');

                containerSite.addClass('hide');
                containerTrip.removeClass('hide');
                containerCreate.addClass('hide');

            });

            buttonCreate.on('click', function (e) {
                buttonSite.removeClass('active');
                buttonTrip.removeClass('active');
                buttonCreate.addClass('active');

                containerSite.addClass('hide');
                containerTrip.addClass('hide');
                containerCreate.removeClass('hide');

            });



        $(".container").empty().append(reviewView);

        return false;
    }

    function handleReview() {
        var title = reviewView.find(["name='title'"]).val();
        var reviewType = reviewView.find(["name='selectType'"]).val();
        var text = reviewView.find(["name='review'"]).val();

        var reviews;
        var review = [title, reviewType, text];

        databaseManager
            .query("INSERT INTO review (title, reviewType, text) VALUES = (?)", [review])
            .done(function () {
                window.alert("U heeft met succes een review gepost als " + reviewType)
            }).fail(function (reason) {
                console.log(reason)
                .find(".error")
                .html("Er is iets misgegaan??? :'(");
    });
        databaseManager
            .query("SELECT title FROM review")
            .done(function (data) {
                reviews = data;
            }).fail(function (reason) {
                console.log(reason)
                    .find("error")
                    . html("Er is iets musgegaan??? :'(");
                console.log(data);
            });
        $(".review").html(data[0].review);

        return false;
    }

    function error() {
    $(".container").html("Failed to load content!")
}

//Run the initialize function to kick things off
initialize();
}