/**
 * Controller for the index page
 *
 * @author Jonathan van Veen
 */
function homeController() {
    var homeView;

    function initialize() {
        $.get("views/home.html")
            .done(setup)
            .fail(error);
    }

    function setup(data) {
        homeView = $(data);

        var currentUser = session.get("userId");
        var currentUserName;

        if (currentUser !== undefined) {
            databaseManager
                .query("SELECT firstName, lastNamePrefix, lastName FROM user WHERE id = ?", [currentUser])
                .done(function (data) {
                    var user = data[0] || {}; //get the first user from the database
                    if (user && user !== undefined) {
                        if (user.lastNamePrefix !== null) {
                            currentUserName = user.firstName + " " + user.lastNamePrefix + " " + user.lastName;
                        } else {
                            currentUserName = user.firstName + " " + user.lastName;
                        }
                    }
                    homeView.find("h1 span.user").html((currentUserName ? ', ' + currentUserName : ''));
                }).fail(function (reason) {
                console.log(reason);
            });
        }

        homeView.find("a").on("click", handleClick);

        $(".container").empty().append(homeView);
    }

    function error() {
        $(".container").html("Failed to load Home!");
    }

    function handleClick() {
    //Get the data-controller from the clicked element (this)
    var controller = $(this).attr("data-controller");
    console.log(controller);

    //Pass the action to a new function for further processing.
    loadController(controller);

    //return false to prevent reloading the page.
    return false;
    }

    initialize();
}